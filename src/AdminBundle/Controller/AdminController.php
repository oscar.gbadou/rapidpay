<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\Admin;
use UserBundle\Form\AdminType;

class AdminController extends Controller
{
  public function adminsAction()
  {
    $em = $this->getDoctrine()->getManager();
    $admins = $em->getRepository("UserBundle:Admin")->findAll();
    return $this->render('AdminBundle:Admin:admins.html.twig', array(
      'admins'=>$admins
    ));
  }

  public function addAdminAction()
  {

    return $this->render('AdminBundle:Admin:addAdmin.html.twig');
  }

  public function saveAdminAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $admin = $em->getRepository("UserBundle:Admin")->findOneByEmail($request->get('email'));
    if($admin){
      $this->get('session')->getFlashBag()->add('error', 'Un compte existe deja avec cette adresse email');
      return $this->redirect($this->generateUrl('admin_admins'));
    }else{
      $pass = rand(100000, 999999);
      $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
      $discriminator->setClass('UserBundle\Entity\Admin', true);
      $userManager = $this->container->get('pugx_user_manager');
      $newUserM = $userManager->createUser();
      $newUserM->setNom($request->get('nom'));
      $newUserM->setEmail($request->get('email'));
      $newUserM->setPlainPassword($pass);
      $newUserM->setPwdprovisoire($pass);
      $newUserM->setTelephone($request->get('telephone'));
      $newUserM->setEnabled(true);
      $newUserM->addRole('ROLE_ADMIN');
      $userManager->updateUser($newUserM, true);

      $this->sendEmailInfoCompte($request->get('email'), $request->get('nom'), $pass);

      $this->get('session')->getFlashBag()->add('success', 'Admin cree avec succes');
      return $this->redirect($this->generateUrl('admin_admins'));
    }
  }

  public function sendEmailInfoCompte($email, $nom, $pass){
    $sendEmail = \Swift_Message::newInstance()->setContentType('text/html')
            ->setSubject('Votre compte sur Rapid Pay')
            ->setFrom(array('no-reply@zemexpress.com' => 'RAPID PAY'))
            ->setTo($email)
            ->setBody($this->renderView('AdminBundle:Admin:email_info_compte.html.twig', array(
                'email' => $email,
                'nom' => $nom,
                'pwd' => $pass
    )));
    $this->get('mailer')->send($sendEmail);
  }

  public function editAdminAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $admin = $em->getRepository("UserBundle:Admin")->find($id);
    if($admin){
      $form = $this->createForm(new AdminType(), $admin);
      $form->handleRequest($request);

      if ($form->isValid()) {
        $em->persist($admin);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Modification effectuee avec succes');
        return $this->redirect($this->generateUrl('admin_admins'));
      }
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Cet admin n\'existe pas');
      return $this->redirect($this->generateUrl('admin_admins'));
    }
    return $this->render('AdminBundle:Admin:editAdmin.html.twig', array(
        'form' => $form->createView(),
    ));
  }

  public function deleteAdminAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $admin = $em->getRepository("UserBundle:Admin")->find($id);
    if($admin){
        $em->remove($admin);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Admin supprime avec succes');
        return $this->redirect($this->generateUrl('admin_admins'));
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Cet admin n\'existe pas');
      return $this->redirect($this->generateUrl('admin_admins'));
    }
  }
}
