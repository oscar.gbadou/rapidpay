<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\Caissier;
use UserBundle\Form\CaissierType;

class CaissierController extends Controller
{

  public function testEmailAction(){
    $email = \Swift_Message::newInstance()->setContentType('text/html')
    ->setSubject('Votre compte sur Rapid Pay')
    ->setFrom(array('no-reply@zemexpress.com' => 'RAPID PAY'))
    ->setTo('maxxhount@gmail.com')
    ->setBody($this->renderView('AdminBundle:Admin:test_email.html.twig'));
    $this->get('mailer')->send($email);
    return $this->render('AdminBundle:Caissier:upload.html.twig');
  }

  public function caissiersAction()
  {
    $em = $this->getDoctrine()->getManager();
    $caissiers = $em->getRepository("UserBundle:Caissier")->findAll();
    return $this->render('AdminBundle:Caissier:caissiers.html.twig', array(
      'caissiers'=>$caissiers
    ));
  }

  public function uploadAction()
  {
    return $this->render('AdminBundle:Caissier:upload.html.twig');
  }

  public function addCaissierAction()
  {
    return $this->render('AdminBundle:Caissier:addCaissier.html.twig');
  }

  public function saveUploadAction()
  {
    $em = $this->getDoctrine()->getManager();
    if (isset($_FILES['file'])) {
      $tmp = $_FILES['file']['tmp_name'];
      $err = $_FILES['file']['error'];
      if ($err) {
        die("erreur de chargement du fichier");
      } else {
        $fp = fopen($tmp, 'r');
        $compteur = 0;
        while ($line = fgetcsv($fp, 1000)) {
          $compteur++;
          if($compteur>1){
            $tab = explode(";", $line[0]);
            $nom = $tab[0];
            $prenom = $tab[1];
            $matricule = $tab[2];
            $email = $tab[3];
            $telephone = $tab[4];
            $agence = $tab[5];
            $numeroCompte = $tab[6];
            $oldCassier = $em->getRepository('UserBundle:Caissier')->findOneByEmail($email);
            if(!$oldCassier){
              $pass = rand(100000, 999999);
              $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
              $discriminator->setClass('UserBundle\Entity\Caissier', false);
              $userManager = $this->container->get('pugx_user_manager');
              $newUserM = $userManager->createUser();
              $newUserM->setNom($nom.' '.$prenom);
              $newUserM->setEmail($email);
              $newUserM->setPlainPassword($pass);
              $newUserM->setPwdprovisoire($pass);
              $newUserM->setTelephone($telephone);
              $newUserM->setMatricule($matricule);
              $newUserM->setNumeroCompte($numeroCompte);
              $newUserM->setAgence($agence);
              $newUserM->setEnabled(true);
              $newUserM->addRole('ROLE_CAISSIER');
              $userManager->updateUser($newUserM, true);

              $this->sendEmailInfoCompte($email, $nom.' '.$prenom, $pass);
            }
          }
        }
        $this->get('session')->getFlashBag()->add('success', 'Caissiers ajoutes avec succes');
        return $this->redirect($this->generateUrl('admin_caissiers'));
      }
    }
    return $this->redirect($this->generateUrl('admin_caissiers'));
  }

  public function saveCaissierAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $caissier = $em->getRepository("UserBundle:Caissier")->findOneByEmail($request->get('email'));
    if($caissier){
      $this->get('session')->getFlashBag()->add('error', 'Un compte existe deja avec cette adresse email');
      return $this->redirect($this->generateUrl('admin_caissiers'));
    }else{
      $pass = rand(100000, 999999);
      $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
      $discriminator->setClass('UserBundle\Entity\Caissier', false);
      $userManager = $this->container->get('pugx_user_manager');
      $newUserM = $userManager->createUser();
      $newUserM->setNom($request->get('nom'));
      $newUserM->setEmail($request->get('email'));
      $newUserM->setPlainPassword($pass);
      $newUserM->setPwdprovisoire($pass);
      $newUserM->setTelephone($request->get('telephone'));
      $newUserM->setMatricule($request->get('matricule'));
      $newUserM->setNumeroCompte($request->get('numeroCompte'));
      $newUserM->setAgence($request->get('agence'));
      $newUserM->setEnabled(true);
      $newUserM->addRole('ROLE_CAISSIER');
      $userManager->updateUser($newUserM, true);

      $this->sendEmailInfoCompte($request->get('email'), $request->get('nom'), $pass);

      $this->get('session')->getFlashBag()->add('success', 'Caissier cree avec succes');
      return $this->redirect($this->generateUrl('admin_caissiers'));
    }
  }

  public function sendEmailInfoCompte($email, $nom, $pass){
    $sendEmail = \Swift_Message::newInstance()->setContentType('text/html')
    ->setSubject('Votre compte sur Rapid Pay')
    ->setFrom(array('no-reply@zemexpress.com' => 'RAPID PAY'))
    ->setTo($email)
    ->setBody($this->renderView('AdminBundle:Admin:email_info_compte.html.twig', array(
      'email' => $email,
      'nom' => $nom,
      'pwd' => $pass
    )));
    $this->get('mailer')->send($sendEmail);
  }

  public function editCaissierAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $caissier = $em->getRepository("UserBundle:Caissier")->find($id);
    if($caissier){
      $form = $this->createForm(new CaissierType(), $caissier);
      $form->handleRequest($request);

      if ($form->isValid()) {
        $em->persist($caissier);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Modification effectuee avec succes');
        return $this->redirect($this->generateUrl('admin_caissiers'));
      }
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Ce caissier n\'existe pas');
      return $this->redirect($this->generateUrl('admin_caissiers'));
    }
    return $this->render('AdminBundle:Caissier:editCaissier.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function deleteCaissierAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $caissier = $em->getRepository("UserBundle:Caissier")->find($id);
    if($caissier){
      $em->remove($caissier);
      $em->flush();
      $this->get('session')->getFlashBag()->add('success', 'Caissier supprime avec succes');
      return $this->redirect($this->generateUrl('admin_caissiers'));
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Cet admin n\'existe pas');
      return $this->redirect($this->generateUrl('admin_caissiers'));
    }
  }
}
