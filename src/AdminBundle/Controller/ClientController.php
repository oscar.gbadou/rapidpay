<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Client;
use AdminBundle\Form\ClientType;
use AdminBundle\Fpdf\FPDF;

class ClientController extends Controller
{
  public function testPdfAction(){
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',16);
    $pdf->Cell(40,10,'Hello World !');
    $pdf->Output();
    die("");
  }

  public function listAction()
  {
    $em = $this->getDoctrine()->getManager();
    $clients = $em->getRepository('AdminBundle:Client')->findAll();
    return $this->render('AdminBundle:Client:list.html.twig', array(
      'clients'=>$clients
    ));
  }

  public function listForCaissierAction()
  {
    $em = $this->getDoctrine()->getManager();
    $clients = $em->getRepository('AdminBundle:Client')->findAll();
    return $this->render('AdminBundle:Client:listForCaissier.html.twig', array(
      'clients'=>$clients
    ));
  }

  public function addAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $client = new Client();
    $form = $this->createForm(new ClientType(), $client);
    $form->handleRequest($request);
    if ($form->isValid()) {
      if(trim($client->getNom()) != "" || trim($client->getTelephone()) != "" || trim($client->getNumeroCompte()) != "" || trim($client->getEmail()) != ""){
        $client->setModePaiement($request->get('modePaiement'));
        if($request->get('modePaiement') == "taux"){
          $client->setTaux($request->get('taux'));
        }
        $em->persist($client);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Client ajoute avec succes');
        return $this->redirect($this->generateUrl('admin_clients'));
      }else{
        $this->get('session')->getFlashBag()->add('error', 'Renseigner les champs marque d\'une *');
        return $this->redirect($this->generateUrl('admin_add_client'));
      }
    }
    return $this->render('AdminBundle:Client:add.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function editAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $client = $em->getRepository('AdminBundle:Client')->find($id);
    if($client){
      $form = $this->createForm(new ClientType(), $client);
      $form->handleRequest($request);
      if ($form->isValid()) {
        if($request->get('modePaiement')){
          $client->setModePaiement($request->get('modePaiement'));
        }
        $em->persist($client);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Client modifie avec succes');
        return $this->redirect($this->generateUrl('admin_clients'));
      }
      return $this->render('AdminBundle:Client:add.html.twig', array(
        'form' => $form->createView(),
      ));
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Ce client n\'existe pas');
      return $this->redirect($this->generateUrl('admin_clients'));
    }
  }

  public function deleteAction($id){
    $em = $this->getDoctrine()->getManager();
    $client = $em->getRepository('AdminBundle:Client')->find($id);
    if($client){
      $em->remove($client);
      $em->flush();
      $this->get('session')->getFlashBag()->add('success', 'Client supprime avec succes');
      return $this->redirect($this->generateUrl('admin_clients'));
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Ce client n\'existe pas');
      return $this->redirect($this->generateUrl('admin_clients'));
    }
  }
}
