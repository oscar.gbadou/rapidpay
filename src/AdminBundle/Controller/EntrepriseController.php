<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\Entreprise;
use UserBundle\Form\EntrepriseType;

class EntrepriseController extends Controller
{
  public function entreprisesAction()
  {
    $em = $this->getDoctrine()->getManager();
    $entreprises = $em->getRepository("UserBundle:Entreprise")->findAll();
    return $this->render('AdminBundle:Entreprise:entreprises.html.twig', array(
      'entreprises'=>$entreprises
    ));
  }

  public function addEntrepriseAction()
  {
    $em = $this->getDoctrine()->getManager();
    $clients = $em->getRepository("AdminBundle:Client")->findAll();
    return $this->render('AdminBundle:Entreprise:addEntreprise.html.twig', array(
      'clients'=>$clients
    ));
  }

  public function saveEntrepriseAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $entreprise = $em->getRepository("UserBundle:Entreprise")->findOneByEmail($request->get('email'));
    if($entreprise){
      $this->get('session')->getFlashBag()->add('error', 'Un compte existe deja avec cette adresse email');
      return $this->redirect($this->generateUrl('admin_entreprises'));
    }else{
      $pass = rand(100000, 999999);
      $client = $em->getRepository("AdminBundle:Client")->find($request->get('client'));
      $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
      $discriminator->setClass('UserBundle\Entity\Entreprise', false);
      $userManager = $this->container->get('pugx_user_manager');
      $newUserM = $userManager->createUser();
      $newUserM->setNom($request->get('nom'));
      $newUserM->setEmail($request->get('email'));
      $newUserM->setPlainPassword($pass);
      $newUserM->setPwdprovisoire($pass);
      $newUserM->setTelephone($request->get('telephone'));
      $newUserM->setPoste($request->get('poste'));
      $newUserM->setEnabled(true);
      $newUserM->setClient($client);
      $newUserM->addRole('ROLE_ENTREPRISE');
      $userManager->updateUser($newUserM, true);

      $this->sendEmailInfoCompte($request->get('email'), $request->get('nom'), $pass);

      $this->get('session')->getFlashBag()->add('success', 'Entreprise cree avec succes');
      return $this->redirect($this->generateUrl('admin_entreprises'));
    }
  }

  public function sendEmailInfoCompte($email, $nom, $pass){
    $sendEmail = \Swift_Message::newInstance()->setContentType('text/html')
    ->setSubject('Votre compte sur Rapid Pay')
    ->setFrom(array('no-reply@zemexpress.com' => 'RAPID PAY'))
    ->setTo($email)
    ->setBody($this->renderView('AdminBundle:Admin:email_info_compte.html.twig', array(
      'email' => $email,
      'nom' => $nom,
      'pwd' => $pass
    )));
    $this->get('mailer')->send($sendEmail);
  }

  public function editEntrepriseAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $entreprise = $em->getRepository("UserBundle:Entreprise")->find($id);
    if($entreprise){
      $form = $this->createForm(new EntrepriseType(), $entreprise);
      $form->handleRequest($request);

      if ($form->isValid()) {
        $em->persist($entreprise);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Modification effectuee avec succes');
        return $this->redirect($this->generateUrl('admin_entreprises'));
      }
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Cette entreprise n\'existe pas');
      return $this->redirect($this->generateUrl('admin_entreprises'));
    }
    return $this->render('AdminBundle:Entreprise:editEntreprise.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function deleteEntrepriseAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $entreprise = $em->getRepository("UserBundle:Entreprise")->find($id);
    if($entreprise){
      $em->remove($entreprise);
      $em->flush();
      $this->get('session')->getFlashBag()->add('success', 'Entreprise supprime avec succes');
      return $this->redirect($this->generateUrl('admin_entreprises'));
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Cette entreprise n\'existe pas');
      return $this->redirect($this->generateUrl('admin_entreprises'));
    }
  }
}
