<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
  public function inscriptionBBAAction(Request $request) {
    $response = new Response();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));

    $response->setContent("SUCCESS");

    $email = \Swift_Message::newInstance()->setContentType('text/html')
    ->setSubject('Inscription au Benin Blog Awards')
    ->setFrom(array('no-reply@zemexpress.com' => 'BENIN BLOG AWARDS'))
    ->setTo('info@beninblogawards.com')
    ->setBody($this->renderView('AdminBundle:Default:inscription_bba.html.twig', array(
      'email' => $request->get('email'),
      'categorie' => $request->get('categorie'),
      'nom' => $request->get('nom'),
      'prenoms' => $request->get('prenoms'),
      'telephone' => $request->get('telephone'),
      'titre' => $request->get('titre'),
      'description' => $request->get('description'),
      'url' => $request->get('url')
    )));
    $result = $this->get('mailer')->send($email);
    //return $this->render('MainBundle:Index:landingPage.html.twig');
    return $response;
  }

  public function contactBBAAction(Request $request) {
    $em = $this->getDoctrine()->getManager();
    $response = new Response();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));

    $response->setContent("SUCCESS");

    $email = \Swift_Message::newInstance()->setContentType('text/html')
    ->setSubject('Contact Benin Blog Awards')
    ->setFrom(array('no-reply@zemexpress.com' => 'BENIN BLOG AWARDS'))
    ->setTo('info@beninblogawards.com')
    //->setTo('oscar.gbadou@gmail.com')
    ->setBody($this->renderView('AdminBundle:Default:contact_bba.html.twig', array(
      'email' => $request->get('email'),
      'name' => $request->get('name'),
      'msg' => $request->get('msg')
    )));
    $this->get('mailer')->send($email);
    return $response;
  }

  public function indexAction()
  {
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    if($this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')){
      $em = $this->getDoctrine()->getManager();
      $compteurs = $em->getRepository('AdminBundle:Compteur')->findByClient($currentUser->getClient());
      $result = array();
      foreach ($compteurs as $c) {
        $montant = 0;
        $factures = $em->getRepository('AdminBundle:Facture')->findByCompteur($c);
        foreach ($factures as $f) {
          $montant += $f->getNetAPayer();
        }
        $result[] = array(
          'site'=>$c->getNomSite(),
          'montant'=>$montant
        );
        $nomSite[] = $c->getNomSite();
        $montantArray[] = $montant;
      }
      if(count($result)>0){
        foreach ($result as $key => $row) {
          $site[$key]  = $row['site'];
          $montantArray[$key] = $row['montant'];
        }
        array_multisort($site, SORT_ASC, $montantArray, SORT_DESC, $result);
      }
      return $this->render('AdminBundle:Client:dashboard.html.twig', array(
        'siteGourmand'=>$result
      ));
    }else{
      return $this->render('AdminBundle:Default:index.html.twig');
    }
  }
}
