<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Entity\Facture;
use AdminBundle\Form\FactureType;
use AdminBundle\Fpdf\FPDF;
use AdminBundle\Fpdf\PDF;
//require_once __DIR__.'/../Excel/reader.php';

class FactureController extends Controller
{
  public function nbreFacturePayeeAction(Request $request){
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $facturesPayee = $em->getRepository('AdminBundle:Facture')
    ->createQueryBuilder('f')
    ->leftJoin('f.compteur', 'c')
    ->leftJoin('c.client', 'cl')
    ->where('cl.id = :id')
    ->andWhere('f.confirmer = TRUE')
    ->setParameter('id', $currentUser->getClient()->getId())
    ->getQuery()
    ->getResult();
    $facturesEnAttente = $em->getRepository('AdminBundle:Facture')
    ->createQueryBuilder('f')
    ->leftJoin('f.compteur', 'c')
    ->leftJoin('c.client', 'cl')
    ->where('cl.id = :id')
    ->andWhere('f.confirmer = FALSE')
    ->setParameter('id', $currentUser->getClient()->getId())
    ->getQuery()
    ->getResult();
    $result = array();
    $result[] = array(
      'label'=>'Payees',
      'value'=>count($facturesPayee)
    );
    $result[] = array(
      'label'=>'En attentes',
      'value'=>count($facturesEnAttente)
    );
    $response = new Response();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    $response->setContent(json_encode($result));
    return $response;
  }

  public function consommationAnnuelleAction(Request $request){
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $now = new \DateTime();
    $annee = $request->get('annee');
    $client = $em->getRepository('AdminBundle:Client')->find($currentUser->getClient()->getId());
    $mois = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
    $result = array();
    foreach ($mois as $m) {
      $montant = 0;
      $factures = $em->getRepository('AdminBundle:Facture')
      ->createQueryBuilder('f')
      ->leftJoin('f.compteur', 'c')
      ->leftJoin('c.client', 'cl')
      ->where('cl.id = :id')
      ->andWhere('f.confirmer = TRUE')
      ->andWhere('f.mois = :mois')
      ->andWhere('f.annee = :annee')
      ->setParameter('id', $currentUser->getClient()->getId())
      ->setParameter('mois', $m)
      ->setParameter('annee', $annee)
      ->getQuery()
      ->getResult();
      foreach ($factures as $f) {
        $montant += $f->getNetAPayer();
      }
      $result[] = array(
        'mois'=>$m,
        'montant'=>$montant
      );
    }
    $response = new Response();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    $response->setContent(json_encode($result));
    return $response;
  }

  public function confirmerFactureAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $factures = $request->get('factures');
    if($factures){
      foreach ($factures as $f) {
        $facture = $em->getRepository('AdminBundle:Facture')->find(intval($f));
        if($facture){
          $facture->setConfirmer(true);
          $em->flush();
        }
      }
    }
    $this->get('session')->getFlashBag()->add('success', 'Facture confirmee avec succes');
    return $this->redirect($this->generateUrl('admin_facture_paye'));
  }

  public function listAction()
  {
    $em = $this->getDoctrine()->getManager();
    $factures = $em->getRepository('AdminBundle:Facture')->findAll();
    return $this->render('AdminBundle:Facture:list.html.twig', array(
      'factures'=>$factures
    ));
  }

  public function factureEnAttenteAction()
  {
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $factures = $em->getRepository('AdminBundle:Facture')
    ->createQueryBuilder('f')
    ->leftJoin('f.compteur', 'c')
    ->leftJoin('c.client', 'cl')
    ->where('cl.id = :id')
    ->andWhere('f.confirmer = :false')
    ->setParameter('id', $currentUser->getClient()->getId())
    ->setParameter('false', false)
    ->getQuery()
    ->getResult();
    return $this->render('AdminBundle:Facture:factureEnAttente.html.twig', array(
      'factures'=>$factures
    ));
  }

  public function facturePayeAction()
  {
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $factures = $em->getRepository('AdminBundle:Facture')
    ->createQueryBuilder('f')
    ->leftJoin('f.compteur', 'c')
    ->leftJoin('c.client', 'cl')
    ->where('cl.id = :id')
    ->andWhere('f.confirmer = :true')
    ->setParameter('id', $currentUser->getClient()->getId())
    ->setParameter('true', true)
    ->getQuery()
    ->getResult();
    return $this->render('AdminBundle:Facture:facturePaye.html.twig', array(
      'factures'=>$factures
    ));
  }

  public function listByClientAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $client = $em->getRepository('AdminBundle:Client')->find($id);
    $factures = $em->getRepository('AdminBundle:Facture')
    ->createQueryBuilder('f')
    ->leftJoin('f.compteur', 'c')
    ->leftJoin('c.client', 'cl')
    ->where('cl.id = :id')
    ->setParameter('id', $id)
    ->getQuery()
    ->getResult();
    return $this->render('AdminBundle:Facture:listByClient.html.twig', array(
      'factures'=>$factures,
      'client'=>$client
    ));
  }

  public function voirFactureAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $client = $em->getRepository('AdminBundle:Client')->find($id);
    $mois = $request->get('mois');
    $annee = $request->get('annee');
    $factures = $em->getRepository('AdminBundle:Facture')
    ->createQueryBuilder('f')
    ->leftJoin('f.compteur', 'c')
    ->leftJoin('c.client', 'cl')
    ->where('cl.id = :id')
    ->andWhere('f.confirmer = TRUE')
    ->andWhere('f.mois = :mois')
    ->andWhere('f.annee = :annee')
    ->setParameter('id', $id)
    ->setParameter('mois', $mois)
    ->setParameter('annee', $annee)
    ->getQuery()
    ->getResult();
    if($request->get('voir_facture')){
      return $this->render('AdminBundle:Facture:voirFacture.html.twig', array(
        'factures'=>$factures,
        'client'=>$client,
        'mois'=>$mois,
        'annee'=>$annee
      ));
    }else{
      $pdf = new PDF();
      // Titres des colonnes
      $header = array('Numero', 'Police', 'Mois', 'Periode', 'Net a payer');
      // Chargement des données
      //$data = $pdf->LoadData('pays.txt');
      $pdf->SetFont('Arial','',14);
      $pdf->AddPage();
      $pdf->Cell(40,10,'Facture des paiements de '.$client->getNom().' ('.$mois.' '.$annee.')');
      $pdf->Ln();
      $pdf->Ln();
      foreach($header as $col)
      $pdf->Cell(35,7,$col, 1);
      $pdf->Ln();
      // Données
      foreach($factures as $f)
      {
        $pdf->Cell(35,6,$f->getNumeroFacture(),1);
        $pdf->Cell(35,6,$f->getNumeroPolice(),1);
        $pdf->Cell(35,6,$f->getMois(),1);
        $pdf->Cell(35,6,$f->getAnnee(),1);
        $pdf->Cell(35,6,$f->getNetAPayer(),1);
        $pdf->Ln();
      }
      $pdf->Output();
      die("");
    }
  }

  public function exporterAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $client = $em->getRepository('AdminBundle:Client')->find($id);
    $mois = $request->get('mois');
    $annee = $request->get('annee');
    $factures = $em->getRepository('AdminBundle:Facture')
    ->createQueryBuilder('f')
    ->leftJoin('f.compteur', 'c')
    ->leftJoin('c.client', 'cl')
    ->where('cl.id = :id')
    ->andWhere('f.confirmer = TRUE')
    ->andWhere('f.mois = :mois')
    ->andWhere('f.annee = :annee')
    ->setParameter('id', $id)
    ->setParameter('mois', $mois)
    ->setParameter('annee', $annee)
    ->getQuery()
    ->getResult();
    if($request->get('liste')){
      return $this->render('AdminBundle:Facture:listConfirmer.html.twig', array(
        'factures'=>$factures,
        'client'=>$client,
        'mois'=>$mois,
        'annee'=>$annee
      ));
    }else{
      $out = fopen("php://output", 'w');
      $dataArray = array(
        'numeroPolice'=>"Numero Police",
        'numeroFacture'=>"Numero facture",
        'mois'=>"Mois",
        'annee'=>"Annee",
        'netAPayer'=>"Net a payer",
        'deadline'=>"Deadline"
      );
      fputcsv($out, $dataArray,"\t");
      foreach ($factures as $data)
      {
        $dataArray = array(
          'numeroPolice'=>$data->getNumeroPolice(),
          'numeroFacture'=>$data->getNumeroFacture(),
          'mois'=>$data->getMois(),
          'annee'=>$data->getAnnee(),
          'netAPayer'=>$data->getNetAPayer(),
          'deadline'=>$data->getDeadline()->format('d-M-Y')
        );
        fputcsv($out, $dataArray,"\t");
      }
      header("Content-Disposition: attachment; filename=\"".$mois."_".$annee.".xls\"");
      header("Content-Type: application/vnd.ms-excel;");
      header("Pragma: no-cache");
      header("Expires: 0");


      fclose($out);
      die("");
      return $this->redirect($this->generateUrl('admin_clients_caissier'));
      /*return $this->render('AdminBundle:Facture:listConfirmer.html.twig', array(
      'factures'=>$factures,
      'client'=>$client,
      'mois'=>$mois,
      'annee'=>$annee
    ));*/
  }
}

public function uploadAction()
{
  return $this->render('AdminBundle:Facture:upload.html.twig');
}

public function saveUploadAction()
{
  $em = $this->getDoctrine()->getManager();
  $moisArray = array();
  $client = null;
  if (isset($_FILES['file'])) {
    $tmp = $_FILES['file']['tmp_name'];
    $err = $_FILES['file']['error'];
    if ($err) {
      die("erreur de chargement du fichier");
    } else {
      $fp = fopen($tmp, 'r');
      $compteur = 0;
      while ($line = fgetcsv($fp, 1000)) {
        $compteur++;
        if($compteur>1){
          $tab = explode(";", $line[0]);
          $numeroPolice = $tab[0];
          $numeroFacture = $tab[1];
          $mois = $tab[2];
          $annee = $tab[3];
          $periodeDebut = $tab[4];
          $periodeFin = $tab[5];
          $netAPayer = $tab[6];
          $deadline = $tab[7];
          $oldFacture = $em->getRepository('AdminBundle:Facture')->findOneByNumeroFacture($numeroFacture);
          if(!$oldFacture){
            if(!in_array($mois, $moisArray)){
              $moisArray[] = $mois;
            }
            $compteur = $em->getRepository('AdminBundle:Compteur')->findOneByNumeroPolice($numeroPolice);
            $client = $compteur->getClient();
            $facture = new Facture();
            $facture->setNumeroPolice($numeroPolice)
            ->setNumeroFacture($numeroFacture)
            ->setPeriodeDebut(date_create_from_format('d/m/Y', $periodeDebut))
            ->setPeriodeFin(date_create_from_format('d/m/Y', $periodeFin))
            ->setNetAPayer($netAPayer)
            ->setDeadline(date_create_from_format('d/m/Y', $deadline))
            ->setMois($mois)
            ->setAnnee($annee)
            ->setCompteur($compteur);
            $em->persist($facture);
            $em->flush();
          }
        }
      }
      foreach ($moisArray as $m) {
        if($client){
          $email = \Swift_Message::newInstance()->setContentType('text/html')
          ->setSubject('Facture du mois de '.$m)
          ->setFrom(array('no-reply@zemexpress.com' => 'RAPID PAY'))
          ->setTo($client->getEmail())
          ->setBody($this->renderView('AdminBundle:Facture:email.html.twig', array(
            'client'=>$client->getNom(),
            'mois'=>$m
          )));
          $this->get('mailer')->send($email);
        }
      }
      $this->get('session')->getFlashBag()->add('success', 'Facture charge avec succes. Une notification a ete envoye au client.');
      return $this->redirect($this->generateUrl('admin_factures'));
    }
  }
  return $this->redirect($this->generateUrl('admin_factures'));
}

public function addAction(Request $request){
  $em = $this->getDoctrine()->getManager();
  $facture = new Facture();
  $form = $this->createForm(new FactureType(), $facture);
  $form->handleRequest($request);
  if ($form->isValid()) {
    $compteur = $em->getRepository('AdminBundle:Compteur')->findOneByNumeroPolice($facture->getNumeroPolice());
    $facture->setCompteur($compteur);
    $em->persist($facture);
    $em->flush();
    if($request->get('notification')){
      $email = \Swift_Message::newInstance()->setContentType('text/html')
      ->setSubject('Facture du mois de '.$facture->getMois())
      ->setFrom(array('no-reply@zemexpress.com' => 'RAPID PAY'))
      ->setTo($compteur->getClient()->getEmail())
      ->setBody($this->renderView('AdminBundle:Facture:email.html.twig', array(
        'client'=>$compteur->getClient()->getNom(),
        'mois'=>$facture->getMois()
      )));
      $this->get('mailer')->send($email);
      $this->get('session')->getFlashBag()->add('success', 'Facture ajoutee avec succes Une notification a etet envoyee au client');
    }else{
      $this->get('session')->getFlashBag()->add('success', 'Facture ajoutee avec succes');
    }
    return $this->redirect($this->generateUrl('admin_factures'));
  }
  return $this->render('AdminBundle:Facture:add.html.twig', array(
    'form' => $form->createView(),
  ));
}

public function editAction(Request $request, $id){
  $em = $this->getDoctrine()->getManager();
  $facture = $em->getRepository('AdminBundle:Facture')->find($id);
  if($facture){
    $form = $this->createForm(new FactureType(), $facture);
    $form->handleRequest($request);
    if ($form->isValid()) {
      $compteur = $em->getRepository('AdminBundle:Compteur')->findOneByNumeroPolice($facture->getNumeroPolice());
      $facture->setCompteur($compteur);
      $em->persist($facture);
      $em->flush();
      $this->get('session')->getFlashBag()->add('success', 'Facture modifiee avec succes');
      return $this->redirect($this->generateUrl('admin_factures'));
    }
    return $this->render('AdminBundle:Facture:add.html.twig', array(
      'form' => $form->createView(),
    ));
  }else{
    $this->get('session')->getFlashBag()->add('error', 'Cette facture n\'existe pas');
    return $this->redirect($this->generateUrl('admin_factures'));
  }
}

public function deleteAction($id){
  $em = $this->getDoctrine()->getManager();
  $facture = $em->getRepository('AdminBundle:Facture')->find($id);
  if($facture){
    $em->remove($facture);
    $em->flush();
    $this->get('session')->getFlashBag()->add('success', 'Facture supprimee avec succes');
    return $this->redirect($this->generateUrl('admin_factures'));
  }else{
    $this->get('session')->getFlashBag()->add('error', 'Cette facture n\'existe pas');
    return $this->redirect($this->generateUrl('admin_factures'));
  }
}
}
