<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\Auditeur;
use UserBundle\Form\AuditeurType;

class AuditeurController extends Controller
{
  public function auditeursAction()
  {
    $em = $this->getDoctrine()->getManager();
    $auditeurs = $em->getRepository("UserBundle:Auditeur")->findAll();
    return $this->render('AdminBundle:Auditeur:auditeurs.html.twig', array(
      'auditeurs'=>$auditeurs
    ));
  }

  public function addAuditeurAction()
  {

    return $this->render('AdminBundle:Auditeur:addAuditeur.html.twig');
  }

  public function saveAuditeurAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $auditeur = $em->getRepository("UserBundle:Auditeur")->findOneByEmail($request->get('email'));
    if($auditeur){
      $this->get('session')->getFlashBag()->add('error', 'Un compte existe deja avec cette adresse email');
      return $this->redirect($this->generateUrl('admin_auditeurs'));
    }else{
      $pass = rand(100000, 999999);
      $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
      $discriminator->setClass('UserBundle\Entity\Auditeur', false);
      $userManager = $this->container->get('pugx_user_manager');
      $newUserM = $userManager->createUser();
      $newUserM->setNom($request->get('nom'));
      $newUserM->setEmail($request->get('email'));
      $newUserM->setPlainPassword($pass);
      $newUserM->setPwdprovisoire($pass);
      $newUserM->setTelephone($request->get('telephone'));
      $newUserM->setEnabled(true);
      $newUserM->addRole('ROLE_AUDITEUR');
      $userManager->updateUser($newUserM, true);

      $this->sendEmailInfoCompte($request->get('email'), $request->get('nom'), $pass);

      $this->get('session')->getFlashBag()->add('success', 'Auditeur cree avec succes');
      return $this->redirect($this->generateUrl('admin_auditeurs'));
    }
  }

  public function sendEmailInfoCompte($email, $nom, $pass){
    $sendEmail = \Swift_Message::newInstance()->setContentType('text/html')
    ->setSubject('Votre compte sur Rapid Pay')
    ->setFrom(array('no-reply@zemexpress.com' => 'RAPID PAY'))
    ->setTo($email)
    ->setBody($this->renderView('AdminBundle:Admin:email_info_compte.html.twig', array(
      'email' => $email,
      'nom' => $nom,
      'pwd' => $pass
    )));
    $this->get('mailer')->send($sendEmail);
  }

  public function editAuditeurAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $auditeur = $em->getRepository("UserBundle:Auditeur")->find($id);
    if($auditeur){
      $form = $this->createForm(new AuditeurType(), $auditeur);
      $form->handleRequest($request);

      if ($form->isValid()) {
        $em->persist($auditeur);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Modification effectuee avec succes');
        return $this->redirect($this->generateUrl('admin_auditeurs'));
      }
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Cet auditeur n\'existe pas');
      return $this->redirect($this->generateUrl('admin_admins'));
    }
    return $this->render('AdminBundle:Auditeur:editAuditeur.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function deleteAuditeurAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $auditeur = $em->getRepository("UserBundle:Auditeur")->find($id);
    if($auditeur){
      $em->remove($auditeur);
      $em->flush();
      $this->get('session')->getFlashBag()->add('success', 'Auditeur supprime avec succes');
      return $this->redirect($this->generateUrl('admin_auditeurs'));
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Cet auditeur n\'existe pas');
      return $this->redirect($this->generateUrl('admin_auditeurs'));
    }
  }
}
