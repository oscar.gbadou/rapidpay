<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Compteur;
use AdminBundle\Form\CompteurType;

class CompteurController extends Controller
{
  public function listAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $client = $em->getRepository('AdminBundle:Client')->find($id);
    $compteurs = $em->getRepository('AdminBundle:Compteur')->findByClient($client);
    return $this->render('AdminBundle:Compteur:list.html.twig', array(
      'compteurs'=>$compteurs
    ));
  }

  public function uploadAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $client = $em->getRepository('AdminBundle:Client')->find($id);
    return $this->render('AdminBundle:Compteur:upload.html.twig', array(
      'client'=>$client
    ));
  }

  public function saveUploadAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $client = $em->getRepository('AdminBundle:Client')->find($id);
    if (isset($_FILES['file'])) {
      $tmp = $_FILES['file']['tmp_name'];
      $err = $_FILES['file']['error'];
      if ($err) {
        die("erreur de chargement du fichier");
      } else {
        $fp = fopen($tmp, 'r');
        $compteur = 0;
        while ($line = fgetcsv($fp, 1000)) {
          $compteur++;
          if($compteur>1){
            $tab = explode(";", $line[0]);
            $numeroPolice = $tab[0];
            $nomSite = $tab[1];
            $oldCompteur = $em->getRepository('AdminBundle:Compteur')->findOneByNumeroPolice(trim($numeroPolice));
            if(!$oldCompteur){
              $compteur = new Compteur();
              $compteur->setNumeroPolice($numeroPolice)
              ->setNomSite($nomSite)
              ->setClient($client);
              $em->persist($compteur);
              $em->flush();
            }
          }
        }
        $this->get('session')->getFlashBag()->add('success', 'Compteur charge avec succes');
        return $this->redirect($this->generateUrl('admin_clients'));
      }
    }
    return $this->redirect($this->generateUrl('admin_clients'));
  }

  public function addAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $client = $em->getRepository('AdminBundle:Client')->find($id);
    $compteur = new Compteur();
    $form = $this->createForm(new CompteurType(), $compteur);
    $form->handleRequest($request);
    if ($form->isValid()) {
      $compteur->setClient($client);
      $em->persist($compteur);
      $em->flush();
      $this->get('session')->getFlashBag()->add('success', 'Compteur ajoute avec succes');
      return $this->redirect($this->generateUrl('admin_clients'));
    }
    return $this->render('AdminBundle:Compteur:add.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function editAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $compteur = $em->getRepository('AdminBundle:Compteur')->find($id);
    if($compteur){
      $form = $this->createForm(new CompteurType(), $compteur);
      $form->handleRequest($request);
      if ($form->isValid()) {
        $em->persist($compteur);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Compteur modifie avec succes');
        return $this->redirect($this->generateUrl('admin_clients'));
      }
      return $this->render('AdminBundle:Compteur:add.html.twig', array(
        'form' => $form->createView(),
      ));
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Ce compteur n\'existe pas');
      return $this->redirect($this->generateUrl('admin_clients'));
    }
  }

  public function deleteAction($id){
    $em = $this->getDoctrine()->getManager();
    $compteur = $em->getRepository('AdminBundle:Compteur')->find($id);
    if($compteur){
      $em->remove($compteur);
      $em->flush();
      $this->get('session')->getFlashBag()->add('success', 'Compteur supprime avec succes');
      return $this->redirect($this->generateUrl('admin_clients'));
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Ce compteur n\'existe pas');
      return $this->redirect($this->generateUrl('admin_clients'));
    }
  }
}
