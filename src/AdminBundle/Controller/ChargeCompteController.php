<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\ChargeCompte;
use AdminBundle\Form\ChargeCompteType;

class ChargeCompteController extends Controller
{
  public function listAction()
  {
    $em = $this->getDoctrine()->getManager();
    $chargeComptes = $em->getRepository('AdminBundle:ChargeCompte')->findAll();
    return $this->render('AdminBundle:ChargeCompte:list.html.twig', array(
      'chargeComptes'=>$chargeComptes
    ));
  }

  public function uploadAction()
  {
    return $this->render('AdminBundle:ChargeCompte:upload.html.twig');
  }

  public function saveUploadAction()
  {
    $em = $this->getDoctrine()->getManager();
    if (isset($_FILES['file'])) {
      $tmp = $_FILES['file']['tmp_name'];
      $err = $_FILES['file']['error'];
      if ($err) {
        die("erreur de chargement du fichier");
      } else {
        $fp = fopen($tmp, 'r');
        $compteur = 0;
        while ($line = fgetcsv($fp, 1000)) {
          $compteur++;
          if($compteur>1){
            $tab = explode(";", $line[0]);
            $nom = $tab[0];
            $prenom = $tab[1];
            $matricule = $tab[2];
            $email = $tab[3];
            $telephone = $tab[4];
            $oldCharge = $em->getRepository('AdminBundle:ChargeCompte')->findOneByEmail($email);
            if(!$oldCharge){
              $chargeCompte = new ChargeCompte();
              $chargeCompte->setNom($nom.' '.$prenom)
              ->setEmail($email)
              ->setTelephone($telephone);
              $em->persist($chargeCompte);
              $em->flush();
            }
          }
        }
        $this->get('session')->getFlashBag()->add('success', 'Charges de compte uploades avec succes');
        return $this->redirect($this->generateUrl('admin_charge_comptes'));
      }
    }
    return $this->redirect($this->generateUrl('admin_charge_comptes'));
  }

  public function addAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $chargeCompte = new ChargeCompte();
    $form = $this->createForm(new ChargeCompteType(), $chargeCompte);
    $form->handleRequest($request);
    if ($form->isValid()) {
      $oldCharge = $em->getRepository('AdminBundle:ChargeCompte')->findOneByEmail($chargeCompte->getEmail());
      if(!$oldCharge){
        $em->persist($chargeCompte);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Charge de compte ajoute avec succes');
        return $this->redirect($this->generateUrl('admin_charge_comptes'));
      }else{
        $this->get('session')->getFlashBag()->add('error', 'Cet email a deja ete utilise pour un charge de compte');
        return $this->redirect($this->generateUrl('admin_charge_comptes'));
      }

    }
    return $this->render('AdminBundle:ChargeCompte:add.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function editAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $chargeCompte = $em->getRepository('AdminBundle:ChargeCompte')->find($id);
    if($chargeCompte){
      $form = $this->createForm(new ChargeCompteType(), $chargeCompte);
      $form->handleRequest($request);
      if ($form->isValid()) {
        $em->persist($chargeCompte);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Charge de compte ajoute avec succes');
        return $this->redirect($this->generateUrl('admin_charge_comptes'));
      }
      return $this->render('AdminBundle:ChargeCompte:add.html.twig', array(
        'form' => $form->createView(),
      ));
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Ce charge de compte n\'existe pas');
      return $this->redirect($this->generateUrl('admin_charge_comptes'));
    }
  }

  public function deleteAction($id){
    $em = $this->getDoctrine()->getManager();
    $chargeCompte = $em->getRepository('AdminBundle:ChargeCompte')->find($id);
    if($chargeCompte){
        $em->remove($chargeCompte);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Charge de compte supprime avec succes');
        return $this->redirect($this->generateUrl('admin_charge_comptes'));
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Ce charge de compte n\'existe pas');
      return $this->redirect($this->generateUrl('admin_charge_comptes'));
    }
  }
}
