<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Compteur
 *
 * @ORM\Table(name="compteur")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\CompteurRepository")
 */
class Compteur
{
    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Client")
     */
    private $client;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroPolice", type="string", length=255)
     */
    private $numeroPolice;

    /**
     * @var string
     *
     * @ORM\Column(name="nomSite", type="string", length=255)
     */
    private $nomSite;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    public function __contruct(){
      $this->nom = $this->numeroPolice.' - '.$this->nomSite;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroPolice
     *
     * @param string $numeroPolice
     *
     * @return Compteur
     */
    public function setNumeroPolice($numeroPolice)
    {
        $this->numeroPolice = $numeroPolice;

        return $this;
    }

    /**
     * Get numeroPolice
     *
     * @return string
     */
    public function getNumeroPolice()
    {
        return $this->numeroPolice;
    }

    /**
     * Set nomSite
     *
     * @param string $nomSite
     *
     * @return Compteur
     */
    public function setNomSite($nomSite)
    {
        $this->nomSite = $nomSite;
        $this->nom = $this->numeroPolice.' - '.$this->nomSite;
        return $this;
    }

    /**
     * Get nomSite
     *
     * @return string
     */
    public function getNomSite()
    {
        return $this->nomSite;
    }

    /**
     * Set client
     *
     * @param \AdminBundle\Entity\Client $client
     *
     * @return Compteur
     */
    public function setClient(\AdminBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AdminBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Compteur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
}
