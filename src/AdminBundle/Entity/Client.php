<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\ChargeCompte")
     */
    private $chargeCompteBank;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroCompte", type="string", length=255)
     */
    private $numeroCompte;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroAbonne", type="string", length=255)
     */
    private $numeroAbonne;

    /**
     * @var string
     *
     * @ORM\Column(name="codeRegroupement", type="string", length=255)
     */
    private $codeRegroupement;

    /**
     * @var string
     *
     * @ORM\Column(name="nomChargeCompte", type="string", length=255)
     */
    private $nomChargeCompte;

    /**
     * @var string
     *
     * @ORM\Column(name="telephoneChargeCompte", type="string", length=255)
     */
    private $telephoneChargeCompte;

    /**
     * @var string
     *
     * @ORM\Column(name="mailChargeCompte", type="string", length=255)
     */
    private $mailChargeCompte;

    /**
     * @var string
     *
     * @ORM\Column(name="modePaiement", type="string", length=255)
     */
    private $modePaiement;

    /**
     * @var int
     *
     * @ORM\Column(name="taux", type="integer", nullable=true)
     */
    private $taux;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Client
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Client
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set numeroCompte
     *
     * @param string $numeroCompte
     *
     * @return Client
     */
    public function setNumeroCompte($numeroCompte)
    {
        $this->numeroCompte = $numeroCompte;

        return $this;
    }

    /**
     * Get numeroCompte
     *
     * @return string
     */
    public function getNumeroCompte()
    {
        return $this->numeroCompte;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Client
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set numeroAbonne
     *
     * @param string $numeroAbonne
     *
     * @return Client
     */
    public function setNumeroAbonne($numeroAbonne)
    {
        $this->numeroAbonne = $numeroAbonne;

        return $this;
    }

    /**
     * Get numeroAbonne
     *
     * @return string
     */
    public function getNumeroAbonne()
    {
        return $this->numeroAbonne;
    }

    /**
     * Set codeRegroupement
     *
     * @param string $codeRegroupement
     *
     * @return Client
     */
    public function setCodeRegroupement($codeRegroupement)
    {
        $this->codeRegroupement = $codeRegroupement;

        return $this;
    }

    /**
     * Get codeRegroupement
     *
     * @return string
     */
    public function getCodeRegroupement()
    {
        return $this->codeRegroupement;
    }

    /**
     * Set nomChargeCompte
     *
     * @param string $nomChargeCompte
     *
     * @return Client
     */
    public function setNomChargeCompte($nomChargeCompte)
    {
        $this->nomChargeCompte = $nomChargeCompte;

        return $this;
    }

    /**
     * Get nomChargeCompte
     *
     * @return string
     */
    public function getNomChargeCompte()
    {
        return $this->nomChargeCompte;
    }

    /**
     * Set telephoneChargeCompte
     *
     * @param string $telephoneChargeCompte
     *
     * @return Client
     */
    public function setTelephoneChargeCompte($telephoneChargeCompte)
    {
        $this->telephoneChargeCompte = $telephoneChargeCompte;

        return $this;
    }

    /**
     * Get telephoneChargeCompte
     *
     * @return string
     */
    public function getTelephoneChargeCompte()
    {
        return $this->telephoneChargeCompte;
    }

    /**
     * Set mailChargeCompte
     *
     * @param string $mailChargeCompte
     *
     * @return Client
     */
    public function setMailChargeCompte($mailChargeCompte)
    {
        $this->mailChargeCompte = $mailChargeCompte;

        return $this;
    }

    /**
     * Get mailChargeCompte
     *
     * @return string
     */
    public function getMailChargeCompte()
    {
        return $this->mailChargeCompte;
    }

    /**
     * Set modePaiement
     *
     * @param string $modePaiement
     *
     * @return Client
     */
    public function setModePaiement($modePaiement)
    {
        $this->modePaiement = $modePaiement;

        return $this;
    }

    /**
     * Get modePaiement
     *
     * @return string
     */
    public function getModePaiement()
    {
        return $this->modePaiement;
    }

    /**
     * Set chargeCompteBank
     *
     * @param \AdminBundle\Entity\ChargeCompte $chargeCompteBank
     *
     * @return Client
     */
    public function setChargeCompteBank(\AdminBundle\Entity\ChargeCompte $chargeCompteBank = null)
    {
        $this->chargeCompteBank = $chargeCompteBank;

        return $this;
    }

    /**
     * Get chargeCompteBank
     *
     * @return \AdminBundle\Entity\ChargeCompte
     */
    public function getChargeCompteBank()
    {
        return $this->chargeCompteBank;
    }

    /**
     * Set taux
     *
     * @param integer $taux
     *
     * @return Client
     */
    public function setTaux($taux)
    {
        $this->taux = $taux;

        return $this;
    }

    /**
     * Get taux
     *
     * @return integer
     */
    public function getTaux()
    {
        return $this->taux;
    }
}
