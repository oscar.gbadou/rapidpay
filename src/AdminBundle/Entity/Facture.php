<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Facture
 *
 * @ORM\Table(name="facture")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\FactureRepository")
 */
class Facture
{
    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Compteur")
     */
    private $compteur;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroPolice", type="string", length=255)
     */
    private $numeroPolice;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroFacture", type="string", length=255)
     */
    private $numeroFacture;

    /**
     * @var string
     *
     * @ORM\Column(name="mois", type="string", length=255)
     */
    private $mois;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="string", length=255)
     */
    private $annee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="periodeDebut", type="datetime")
     */
    private $periodeDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="periodeFin", type="datetime")
     */
    private $periodeFin;

    /**
     * @var int
     *
     * @ORM\Column(name="netAPayer", type="integer")
     */
    private $netAPayer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime")
     */
    private $deadline;

    /**
    * @var bool
    *
    * @ORM\Column(name="confirmer", type="boolean")
    */
    private $confirmer;

    public function __construct(){
      $this->confirmer = false;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroPolice
     *
     * @param string $numeroPolice
     *
     * @return Facture
     */
    public function setNumeroPolice($numeroPolice)
    {
        $this->numeroPolice = $numeroPolice;

        return $this;
    }

    /**
     * Get numeroPolice
     *
     * @return string
     */
    public function getNumeroPolice()
    {
        return $this->numeroPolice;
    }

    /**
     * Set numeroFacture
     *
     * @param string $numeroFacture
     *
     * @return Facture
     */
    public function setNumeroFacture($numeroFacture)
    {
        $this->numeroFacture = $numeroFacture;

        return $this;
    }

    /**
     * Get numeroFacture
     *
     * @return string
     */
    public function getNumeroFacture()
    {
        return $this->numeroFacture;
    }

    /**
     * Set periodeDebut
     *
     * @param \DateTime $periodeDebut
     *
     * @return Facture
     */
    public function setPeriodeDebut($periodeDebut)
    {
        $this->periodeDebut = $periodeDebut;

        return $this;
    }

    /**
     * Get periodeDebut
     *
     * @return \DateTime
     */
    public function getPeriodeDebut()
    {
        return $this->periodeDebut;
    }

    /**
     * Set periodeFin
     *
     * @param \DateTime $periodeFin
     *
     * @return Facture
     */
    public function setPeriodeFin($periodeFin)
    {
        $this->periodeFin = $periodeFin;

        return $this;
    }

    /**
     * Get periodeFin
     *
     * @return \DateTime
     */
    public function getPeriodeFin()
    {
        return $this->periodeFin;
    }

    /**
     * Set netAPayer
     *
     * @param integer $netAPayer
     *
     * @return Facture
     */
    public function setNetAPayer($netAPayer)
    {
        $this->netAPayer = $netAPayer;

        return $this;
    }

    /**
     * Get netAPayer
     *
     * @return int
     */
    public function getNetAPayer()
    {
        return $this->netAPayer;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     *
     * @return Facture
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set compteur
     *
     * @param \AdminBundle\Entity\Compteur $compteur
     *
     * @return Facture
     */
    public function setCompteur(\AdminBundle\Entity\Compteur $compteur = null)
    {
        $this->compteur = $compteur;

        return $this;
    }

    /**
     * Get compteur
     *
     * @return \AdminBundle\Entity\Compteur
     */
    public function getCompteur()
    {
        return $this->compteur;
    }

    /**
     * Set confirmer
     *
     * @param boolean $confirmer
     *
     * @return Facture
     */
    public function setConfirmer($confirmer)
    {
        $this->confirmer = $confirmer;

        return $this;
    }

    /**
     * Get confirmer
     *
     * @return boolean
     */
    public function getConfirmer()
    {
        return $this->confirmer;
    }

    /**
     * Set annee
     *
     * @param string $annee
     *
     * @return Facture
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set mois
     *
     * @param string $mois
     *
     * @return Facture
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return string
     */
    public function getMois()
    {
        return $this->mois;
    }
}
