<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChargeCompteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nom', 'text', array(
            'label' => ' ',
            'attr' => array(
                'placeholder' => '* Nom & prénoms',
                'class' => 'form-control'
            )
        ))
        ->add('email', 'email', array(
            'label' => ' ',
            'translation_domain' => 'FOSUserBundle',
            'attr' => array(
                'placeholder' => '* Email',
                'class' => 'form-control'
            )
        ))
        ->add('telephone', 'text', array(
            'label' => ' ',
            'attr' => array(
                'placeholder' => '* Téléphone, exple: 229XXXXXXXX',
                'class' => 'form-control',
            )
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\ChargeCompte'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_chargecompte';
    }


}
