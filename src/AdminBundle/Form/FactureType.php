<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FactureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('numeroPolice', 'text', array(
          'label' => ' ',
          'attr' => array(
            'class' => 'form-control'
          )
        ))
        ->add('numeroFacture', 'text', array(
          'label' => ' ',
          'attr' => array(
            'class' => 'form-control'
          )
        ))
        ->add('annee', 'text', array(
          'label' => ' ',
          'attr' => array(
            'class' => 'form-control'
          )
        ))
        ->add('mois', 'choice', array(
          'choices'=>array(
            'Janvier'=>'Janvier',
            'Fevrier'=>'Fevrier',
            'Mars'=>'Mars',
            'Avril'=>'Avril',
            'Mai'=>'Mai',
            'Juin'=>'Juin',
            'Juillet'=>'Juillet',
            'Aout'=>'Aout',
            'Septembre'=>'Septembre',
            'Octobre'=>'Octobre',
            'Novembre'=>'Novembre',
            'Decembre'=>'Decembre',
          ),
          'attr'=>array(
            'class'=>'form-control'
          )
        ))
        /*->add('mois', 'birthday', array(
          'label' => ' ',
          'attr' => array(
            'class' => 'form-control'
          )
        ))*/
        ->add('periodeDebut', 'birthday', array(
          'label' => ' ',
          'attr' => array(
            'class' => 'form-control'
          )
        ))
        ->add('periodeFin', 'birthday', array(
          'label' => ' ',
          'attr' => array(
            'class' => 'form-control'
          )
        ))
        ->add('netAPayer', 'integer', array(
          'label' => ' ',
          'attr' => array(
            'class' => 'form-control'
          )
        ))
        ->add('deadline', 'birthday', array(
          'label' => ' ',
          'attr' => array(
            'class' => 'form-control'
          )
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\Facture'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_facture';
    }


}
