<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
  /**
  * {@inheritdoc}
  */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('nom', 'text', array(
      'required' => true,
      'label' => ' ',
      'attr' => array(
        'class' => 'form-control'
      )
    ))
    ->add('telephone', 'text', array(
      'required' => true,
      'label' => ' ',
      'attr' => array(
        'class' => 'form-control'
      )
    ))
    ->add('numeroCompte', 'text', array(
      'required' => true,
      'label' => ' ',
      'attr' => array(
        'class' => 'form-control'
      )
    ))
    ->add('email', 'text', array(
      'required' => true,
      'label' => ' ',
      'attr' => array(
        'class' => 'form-control'
      )
    ))
    ->add('adresse', 'text', array(
      'required' => true,
      'label' => ' ',
      'attr' => array(
        'class' => 'form-control'
      )
    ))
    ->add('numeroAbonne', 'text', array(
      'required' => true,
      'label' => ' ',
      'attr' => array(
        'class' => 'form-control'
      )
    ))
    ->add('codeRegroupement', 'text', array(
      'required' => true,
      'label' => ' ',
      'attr' => array(
        'class' => 'form-control'
      )
    ))
    ->add('nomChargeCompte', 'text', array(
      'required' => true,
      'label' => ' ',
      'attr' => array(
        'class' => 'form-control'
      )
    ))
    ->add('telephoneChargeCompte', 'text', array(
      'required' => true,
      'label' => ' ',
      'attr' => array(
        'class' => 'form-control'
      )
    ))
    ->add('mailChargeCompte', 'text', array(
      'required' => true,
      'label' => ' ',
      'attr' => array(
        'class' => 'form-control'
      )
    ))
    /*->add('modePaiement', 'choice', array(
      'choices' => array(
        'Cash' => 'cash',
        'Par taux' => 'taux'
      ),
      'choices_as_values' => true,
      'multiple'=>false,
      'expanded'=>true
    ))*/
    ->add('chargeCompteBank', 'entity', array(
      'placeholder' => 'Choisir un charge de compte de banque',
      'required' => true,
      'label' => ' ',
      'class' => "AdminBundle:ChargeCompte",
      'choice_label' => "nom",
      'attr' => array(
        'class' => 'form-control'
      )
    ));
  }

  /**
  * {@inheritdoc}
  */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'AdminBundle\Entity\Client'
    ));
  }

  /**
  * {@inheritdoc}
  */
  public function getBlockPrefix()
  {
    return 'adminbundle_client';
  }


}
