<?php

namespace UserBundle\Controller;

use \FOS\UserBundle\Controller\SecurityController as BaseController;

class SecurityController extends BaseController {

    protected function renderLogin(array $data) {
        $template = sprintf('UserBundle:Security:connexion.html.twig');

        return $this->container->get('templating')->renderResponse($template, $data);
    }

}
