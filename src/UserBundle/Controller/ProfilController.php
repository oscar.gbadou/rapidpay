<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProfilController extends Controller
{
  public function editAction(Request $request)
  {
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    $roles = $currentUser->getRoles();
    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository('UserBundle:User')->find($currentUser->getId());
    if($roles[0] == "ROLE_CAISSIER"){
      $form = $this->createFormBuilder($currentUser)
      ->add('nom', 'text', array(
        'label' => 'Nom',
        'attr' => array(
          'class' => 'form-control'
        )
      ))
      ->add('telephone', 'text', array(
        'attr' => array(
          'class' => 'form-control'
        )
      ))
      ->add('email', 'email', array(
        'attr' => array(
          'class' => 'form-control'
        )
      ))
      ->add('matricule', 'text', array(
        'attr' => array(
          'class' => 'form-control'
        )
      ))
      ->add('agence', 'text', array(
        'attr' => array(
          'class' => 'form-control'
        )
      ))
      ->add('numeroCompte', 'text', array(
        'attr' => array(
          'class' => 'form-control'
        )
      ))
      ->getForm();
    }elseif($roles[0] == "ROLE_AUDITEUR" || $roles[0] == "ROLE_ADMIN"){
      $form = $this->createFormBuilder($currentUser)
      ->add('nom', 'text', array(
        'label' => 'Nom',
        'attr' => array(
          'class' => 'form-control'
        )
      ))
      ->add('telephone', 'text', array(
        'attr' => array(
          'class' => 'form-control'
        )
      ))
      ->add('email', 'email', array(
        'attr' => array(
          'class' => 'form-control'
        )
      ))
      ->getForm();
    }elseif($roles[0] == "ROLE_ENTREPRISE"){
      $form = $this->createFormBuilder($currentUser)
      ->add('nom', 'text', array(
          'label' => ' ',
          'attr' => array(
              'placeholder' => '* Nom & prénoms',
              'class' => 'form-control'
          )
      ))
      ->add('email', 'email', array(
          'label' => ' ',
          'translation_domain' => 'FOSUserBundle',
          'attr' => array(
              'placeholder' => '* Email',
              'class' => 'form-control'
          )
      ))
      ->add('poste', 'text', array(
          'label' => ' ',
          'attr' => array(
              'placeholder' => 'Poste',
              'class' => 'form-control'
          )
      ))
      ->add('telephone', 'text', array(
          'label' => ' ',
          'attr' => array(
              'placeholder' => '* Téléphone, exple: 229XXXXXXXX',
              'class' => 'form-control',
          )
      ))
      ->add('client', 'entity', array(
        'placeholder' => 'Choisir une entreprise',
        'required' => true,
        'label' => ' ',
        'class' => "AdminBundle:Client",
        'choice_label' => "nom",
        'attr' => array(
          'class' => 'form-control'
        )
      ))
      ->getForm();
    }

    $form->handleRequest($request);

    if ($form->isValid()) {
      // data is an array with "name", "email", and "message" keys
      $data = $form->getData();
      $user->setNom($data->getNom());
      $user->setEmail($data->getEmail());
      $user->setTelephone($data->getTelephone());
      $em->flush();

      $this->get('session')->getFlashBag()->add('success', ' Profil modifié avec succès');
      return $this->redirect($this->generateUrl('user_edit_profil'));
    }
    return $this->render('UserBundle:Profil:edit.html.twig', array(
      'form' => $form->createView(),
    ));
  }
}
