<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntrepriseType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Nom & prénoms',
                        'class' => 'form-control'
                    )
                ))
                ->add('email', 'email', array(
                    'label' => ' ',
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'placeholder' => '* Email',
                        'class' => 'form-control'
                    )
                ))
                ->add('poste', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Poste',
                        'class' => 'form-control'
                    )
                ))
                ->add('telephone', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Téléphone, exple: 229XXXXXXXX',
                        'class' => 'form-control',
                    )
                ))
                ->add('client', 'entity', array(
                  'placeholder' => 'Choisir une entreprise',
                  'required' => true,
                  'label' => ' ',
                  'class' => "AdminBundle:Client",
                  'choice_label' => "nom",
                  'attr' => array(
                    'class' => 'form-control'
                  )
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\Entreprise'
        ));
    }

}
