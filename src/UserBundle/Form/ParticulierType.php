<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Nom & prénoms',
                        'class' => 'form-control'
                    )
                ))
                ->add('email', 'email', array(
                    'label' => ' ',
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'placeholder' => '* Email',
                        'class' => 'form-control'
                    )
                ))
                ->remove('username', null, array(
                    'label' => ' ',
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'placeholder' => '* Nom d\'utilisateur',
                        'class' => 'form-control'
                    )
                ))
                ->add('plainPassword', 'repeated', array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array(
                        'label' => ' ',
                        'attr' => array(
                            'placeholder' => '* Mot de passe',
                            'class' => 'form-control'
                        )
                    ),
                    'second_options' => array(
                        'label' => ' ',
                        'attr' => array(
                            'placeholder' => '* Resaisir mot de passe',
                            'class' => 'form-control'
                        )
                    ),
                    'invalid_message' => 'fos_user.password.mismatch',
                ))
                ->add('telephone', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Téléphone, exple: 229XXXXXXXX',
                        'class' => 'form-control',
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\Admin'
        ));
    }

}
