<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
* User
*
* @ORM\Table(name="user")
* @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
* @ORM\InheritanceType("JOINED")
* @ORM\DiscriminatorColumn(name="type", type="string")
* @ORM\DiscriminatorMap({"admin" = "Admin", "auditeur" = "Auditeur", "caissier" = "Caissier", "entreprise" = "Entreprise"})
*/
abstract class User extends BaseUser
{
  /**
  * @var int
  *
  * @ORM\Column(name="id", type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  protected $id;

  /**
  * @var string
  *
  * @ORM\Column(name="nom", type="string", length=255, nullable=true)
  */
  private $nom;

  /**
  * @var string
  *
  * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
  */
  private $telephone;

  /**
  * @var string
  *
  * @ORM\Column(name="pwdprovisoire", type="string", length=255, nullable=true)
  */
  private $pwdprovisoire;

  public function __construct()
  {
    parent::__construct();
  }


  /**
  * Get id
  *
  * @return int
  */
  public function getId()
  {
    return $this->id;
  }

  /**
  * Set nom
  *
  * @param string $nom
  *
  * @return User
  */
  public function setNom($nom)
  {
    $this->nom = $nom;

    return $this;
  }

  /**
  * Get nom
  *
  * @return string
  */
  public function getNom()
  {
    return $this->nom;
  }

  /**
  * Set prenom
  *
  * @param string $prenom
  *
  * @return User
  */
  public function setPrenom($prenom)
  {
    $this->prenom = $prenom;

    return $this;
  }

  /**
  * Get prenom
  *
  * @return string
  */
  public function getPrenom()
  {
    return $this->prenom;
  }

  /**
  * Set telephone
  *
  * @param string $telephone
  *
  * @return User
  */
  public function setTelephone($telephone)
  {
    $this->telephone = $telephone;

    return $this;
  }

  /**
  * Get telephone
  *
  * @return string
  */
  public function getTelephone()
  {
    return $this->telephone;
  }

  /**
  * Set profil
  *
  * @param string $profil
  *
  * @return User
  */
  public function setProfil($profil)
  {
    $this->profil = $profil;

    return $this;
  }

  /**
  * Get profil
  *
  * @return string
  */
  public function getProfil()
  {
    return $this->profil;
  }

  public function setEmail($email) {
      parent::setEmail($email);
      $this->setUsername($email);
      return $this;
  }

    /**
     * Set pwdprovisoire
     *
     * @param string $pwdprovisoire
     *
     * @return User
     */
    public function setPwdprovisoire($pwdprovisoire)
    {
        $this->pwdprovisoire = $pwdprovisoire;

        return $this;
    }

    /**
     * Get pwdprovisoire
     *
     * @return string
     */
    public function getPwdprovisoire()
    {
        return $this->pwdprovisoire;
    }
}
