<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;

/**
 * Entreprise
 *
 * @ORM\Table(name="entreprise")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\EntrepriseRepository")
 * @UniqueEntity(fields = "email", targetClass = "UserBundle\Entity\User", message="fos_user.email.already_used")
 */
class Entreprise extends User
{
    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Client")
     */
    private $client;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @var string
    *
    * @ORM\Column(name="poste", type="string", length=255, nullable=true)
    */
    private $poste;

    /**
     * Set poste
     *
     * @param string $poste
     *
     * @return Entreprise
     */
    public function setPoste($poste)
    {
        $this->poste = $poste;

        return $this;
    }

    /**
     * Get poste
     *
     * @return string
     */
    public function getPoste()
    {
        return $this->poste;
    }

    /**
     * Set client
     *
     * @param \AdminBundle\Entity\Client $client
     *
     * @return Entreprise
     */
    public function setClient(\AdminBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AdminBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
