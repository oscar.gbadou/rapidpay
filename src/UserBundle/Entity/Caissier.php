<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;

/**
 * Caissier
 *
 * @ORM\Table(name="caissier")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\CaissierRepository")
 * @UniqueEntity(fields = "email", targetClass = "UserBundle\Entity\User", message="fos_user.email.already_used")
 */
class Caissier extends User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @var string
    *
    * @ORM\Column(name="agence", type="string", length=255, nullable=true)
    */
    private $agence;

    /**
    * @var string
    *
    * @ORM\Column(name="matricule", type="string", length=255, nullable=true)
    */
    private $matricule;

    /**
    * @var string
    *
    * @ORM\Column(name="numeroCompte", type="string", length=255, nullable=true)
    */
    private $numeroCompte;


    /**
     * Set agence
     *
     * @param string $agence
     *
     * @return Caissier
     */
    public function setAgence($agence)
    {
        $this->agence = $agence;

        return $this;
    }

    /**
     * Get agence
     *
     * @return string
     */
    public function getAgence()
    {
        return $this->agence;
    }

    /**
     * Set matricule
     *
     * @param string $matricule
     *
     * @return Caissier
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;

        return $this;
    }

    /**
     * Get matricule
     *
     * @return string
     */
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * Set numeroCompte
     *
     * @param string $numeroCompte
     *
     * @return Caissier
     */
    public function setNumeroCompte($numeroCompte)
    {
        $this->numeroCompte = $numeroCompte;

        return $this;
    }

    /**
     * Get numeroCompte
     *
     * @return string
     */
    public function getNumeroCompte()
    {
        return $this->numeroCompte;
    }
}
